#include "window.hh"
#include "SDL2/SDL_video.h"
#include <sdl_wrapper/sdl_exception.hh>

namespace sdl::video
{
Window::Window(video::Context &, std::string_view title, int x, int y, int w, int h, Uint32 flags)
    : WeakWindow(SDL_CreateWindow(title.data(), x, y, w, h, flags))
{
    if (getHandle() == nullptr)
    {
        throw SDLException("creating window");
    }
}

Window::Window(SDL_Window *handle) : WeakWindow(handle)
{
}

Window::Window(const void *nativeData) : WeakWindow(SDL_CreateWindowFrom(nativeData))
{
    if (getHandle() == nullptr)
    {
        throw SDLException("creating window from native window");
    }
}

Window::~Window()
{
    if (getHandle() != nullptr)
    {
        SDL_DestroyWindow(getHandle());
    }
}

Window::Window(Window &&other) : WeakWindow(other.getHandle())
{
    other.setHandle(nullptr);
}

Window &Window::operator=(Window &&other)
{
    if (&other != this)
    {
        setHandle(other.getHandle());
        other.setHandle(nullptr);
    }
    return *this;
}
} // namespace sdl::video