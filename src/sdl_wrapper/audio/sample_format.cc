#include "sample_format.hh"

namespace sdl::audio
{
SampleFormat::SampleFormat(Uint16 value) : value(value)
{
}

Uint16 SampleFormat::getValue() const
{
    return value;
}
} // namespace sdl::audio

std::ostream &operator<<(std::ostream &os, const sdl::audio::SampleFormat &format)
{
    os << format.value;
    return os;
}

sdl::audio::SampleFormat operator|(const sdl::audio::SampleFormat &a, const sdl::audio::SampleFormat &b)
{
    return sdl::audio::SampleFormat{static_cast<Uint16>(a.getValue() | b.getValue())};
}
sdl::audio::SampleFormat operator&(const sdl::audio::SampleFormat &a, const sdl::audio::SampleFormat &b)
{
    return sdl::audio::SampleFormat{static_cast<Uint16>(a.getValue() & b.getValue())};
}
sdl::audio::SampleFormat operator^(const sdl::audio::SampleFormat &a, const sdl::audio::SampleFormat &b)
{
    return sdl::audio::SampleFormat{static_cast<Uint16>(a.getValue() ^ b.getValue())};
}
bool operator==(const sdl::audio::SampleFormat &a, const sdl::audio::SampleFormat &b)
{
    return a.getValue() == b.getValue();
}
bool operator==(const sdl::audio::SampleFormat &format, Uint16 value)
{
    return format.getValue() == value;
}
bool operator==(Uint16 value, const sdl::audio::SampleFormat &format)
{
    return value == format.getValue();
}